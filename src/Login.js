import axios from "axios";
import React, { useEffect } from "react";
import { useState } from "react";
import { useNavigate } from "react-router-dom";
import "./Login.css";

export default function Login() {
  let navigate = useNavigate();
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [error, setError] = useState(null);
  const [message, setMessage] = useState(false);

  const data = {
    email: email,
    password: password,
  };
  const loginUser = (e) => {
    e.preventDefault();
    axios
      .post("https://thepallete.site/api/user-login", data)
      .then((res) => {
        localStorage.setItem("user-info", JSON.stringify(res.data.data));
        navigate("/");
      })
      .catch((err) => {
        console.log(err);
        setMessage(true);
      });
  };
  return (
    <div>
      <div className="container">
        <div className="row">
          <div className="col-md-6">
            <div className="my-form">
              <h3>Welcome to The Pallete site</h3>
              <div className="text-left">
                <p>
                  Lorem ipsum dolor sit amet consectetur adipisicing elit.
                  Expedita nihil molestiae in nulla doloremque consequatur
                  accusantium recusandae reprehenderit quos ipsam!
                </p>
              </div>
              <form className="my-5" onSubmit={loginUser}>
                {message && (
                  <div className="alert alert-danger" role="alert">
                    Email atau Password Salah..!
                  </div>
                )}

                <div class="col-sm-7 mb-3">
                  <label htmlFor="exampleInputEmail1" class="form-label">
                    Email address
                  </label>
                  <input
                    type="email"
                    class="form-control"
                    id="exampleInputEmail1"
                    aria-describedby="emailHelp"
                    onChange={(e) => setEmail(e.target.value)}
                    required
                  />
                  <div id="emailHelp" class="form-text">
                    We'll never share your email with anyone else.
                  </div>
                </div>
                <div class="col-sm-7 mb-3">
                  <label htmlFor="exampleInputPassword1" class="form-label">
                    Password
                  </label>
                  <input
                    type="password"
                    class="form-control"
                    id="exampleInputPassword1"
                    onChange={(e) => setPassword(e.target.value)}
                    required
                  />
                </div>

                <div className="row">
                  <div className="col-md-5">
                    <button type="submit" class="btn btn-primary">
                      Login
                    </button>
                  </div>
                  <div className="col-md-2 ms-5 my-3">
                    <a
                      href="/register-user"
                      className="me-2 "
                      style={{ textDecoration: "none" }}
                    >
                      {" "}
                      Sign Up
                    </a>
                  </div>
                </div>
              </form>
              <div className="row"></div>
            </div>
          </div>
          <div className="col-md-6">
            <img
              src={`${process.env.PUBLIC_URL}/assets/images/login-image.jpg`}
              alt=""
              style={{ height: "750px", width: "747px" }}
            />
          </div>
        </div>
      </div>
      <footer>
        <div className="row">
          <div className="col-12 bg-primary">
            <p className="text-center" id="date">
              &copy;The Pallete-2022
            </p>
          </div>
        </div>
      </footer>
    </div>
  );
}

import React from 'react';

export default function SuccessPayment() {
  return (
    <div>
      <div className="container mt-0">
        <div className="row mt-0">
          <div className="col-12 ps-0">
            <div className="text-center">
              <img
                src="/assets/images/27.-Payment-Process.png"
                alt=""
                style={{ width: '40%' }}
              />
              <h3
                className=" text-lg text-center"
                style={{ marginTop: '-100px' }}
              >
                Checkout Completed
              </h3>
              <p className=" text-sm">
                Segera lakukan pembayaran dengan cara hubungi <br />
                Admin melalui WhatsApp
              </p>
              <div className="my text-center">
                <a
                  href="https://api.whatsapp.com/send?phone=6287832186926&text=Saya%20tertarik%20untuk%20membeli%20produk%20ini%20segera"
                  className="btn btn-outline-success rounded-2"
                >
                  <div className="row" style={{ maxHeight: '40px' }}>
                    <div className="col-md-3">
                      <img
                        src="/whatsapp.png"
                        alt=""
                        style={{ width: '40px' }}
                      />{' '}
                    </div>
                    <div className="col-md-7 d-flex  align-items-center">
                      <p className="text-start pt-2"> Hubungi&nbsp;Admin</p>
                    </div>
                  </div>
                </a>
              </div>

              <div className="my text-center mt-2">
                <a href="/" className="btn btn-outline-primary rounded-2">
                  <div className="row" style={{ maxHeight: '40px' }}>
                    <div className="col-md-3">
                      <img
                        src="/shopping-cart.png"
                        alt=""
                        style={{ width: '40px' }}
                      />{' '}
                    </div>
                    <div className="col-md-7 d-flex  align-items-center">
                      <p className="text-start pt-2"> Product&nbsp;Lainnya</p>
                    </div>
                  </div>
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

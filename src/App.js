import axios from "axios";
import { useEffect, useState } from "react";
import { BrowserRouter as Router, Route, Routes } from "react-router-dom";
import "./App.css";
import Nav from "./components/Nav";
import Finish from "./Finish";
import Home from "./Home";
import Kantor from "./Kantor";
import Krawangan from "./Krawangan";
import Lampu from "./Lampu";
import ListProduct from "./ListProduct";
import Login from "./Login";
import Order from "./Order";
import Page404 from "./Page404";
import Paid from "./Paid";
import ProductDetail from "./ProductDetail";
import Register from "./Register";
import Sculpture from "./Sculpture";
import SuccessPayment from "./SuccessPayment";
import Helmet from "react-helmet";
import Profile from "./components/Profile";
import ChangePass from "./components/ChangePass";

function App() {
  const [product, setProduct] = useState([]);

  useEffect(() => {
    getProduct();

    // return () => {
    //   cleanup
    // }
  }, []);

  const getProduct = async () => {
    let result = await axios.get("https://thepallete.site/api/products");
    result = await result.data.data;
    setProduct(result);
  };

  // const login = async () => {
  //   let result = await axios
  //     .post('http://127.0.0.1:8000/api/login')
  //     .then((res) => {});
  // };

  return (
    <div>
      <Helmet>
        <meta charSet="utf-8" />
        <title>The Pallete</title>
        <meta name="description" content="The pallete e-commerce" />
        <link
          rel="thepallete-ecommerce"
          href="https://thepallete.vercel.app/"
        />
      </Helmet>
      <Router>
        {/* <Nav /> */}
        <Routes>
          <Route path="/" element={<Home productList={product} />} />
          <Route
            path="/all-product"
            element={<ListProduct productList={product} />}
          />

          <Route path="/krawangan" element={<Krawangan />} />
          <Route path="/lampu" element={<Lampu />} />
          <Route path="/kantor" element={<Kantor />} />
          <Route path="/sculpture" element={<Sculpture />} />
          <Route path="/success-payment" element={<SuccessPayment />} />
          <Route
            path="/detail/:Id/product"
            element={<ProductDetail productList={product} />}
          />
          <Route path="/profile/:Id" element={<Profile />}></Route>
          <Route path="/change-password/:Id" element={<ChangePass />}></Route>
          <Route path="/login-user" element={<Login />}></Route>
          <Route path="/register-user" element={<Register />} />
          <Route path="/my-order/:Id" element={<Order productList={product} />}>
            {/* <Route path="/sending-order" element></Route> */}
          </Route>
          <Route path="/paid-order/:Id" element={<Paid />} />
          <Route path="/finish-order/:Id" element={<Finish />} />

          <Route path="*" element={<Page404 />}></Route>
        </Routes>
      </Router>
    </div>
  );
}

export default App;

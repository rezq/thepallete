import axios from "axios";
import React from "react";
import { useState } from "react";
import { useNavigate } from "react-router-dom";

export default function Register() {
  const navigate = useNavigate();
  const [passError, setPassErr] = useState(false);
  const [smallError, setSmallError] = useState(false);
  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [phone, setPhone] = useState("");
  const [address, setAddress] = useState("");
  const [password, setPassword] = useState("");
  const body = {
    name: name,
    email: email,
    phone: phone,
    address: address,
    password: password,
  };
  const handleRegister = (e) => {
    e.preventDefault();
    console.log(body);
    if (password.length < 6) {
      setSmallError(true);
      console.log("stop small");
      return false;
    }

    axios
      .post("https://thepallete.site/api/user-register", body)
      .then((res) => {
        localStorage.setItem("user-info", JSON.stringify(res.data.data));
        navigate("/");
      })
      .catch((err) => {
        console.log(err);
      });
  };

  return (
    <div>
      <div className="container">
        <div className="row">
          <div className="col-md-6">
            <div className="my-form">
              <h3>Welcome to The Pallete site</h3>
              <div className="text-left">
                <p>
                  Lorem ipsum dolor sit amet consectetur adipisicing elit.
                  Expedita nihil molestiae in nulla doloremque consequatur
                  accusantium recusandae reprehenderit quos ipsam!
                </p>
              </div>
              <form className="my-2" onSubmit={handleRegister}>
                <div className="col-sm-7 mb-3">
                  <label htmlFor="exampleInputEmail1" className="form-label">
                    Email address
                  </label>
                  <input
                    type="email"
                    className="form-control"
                    name="email"
                    id="exampleInputEmail1"
                    aria-describedby="emailHelp"
                    onChange={(e) => setEmail(e.target.value)}
                    required
                  />
                  <div id="emailHelp" className="form-text">
                    We'll never share your email with anyone else.
                  </div>
                </div>
                <div className="col-sm-7 mb-2">
                  <label htmlFor="exampleInputEmail1" className="form-label">
                    Full Name
                  </label>
                  <input
                    type="text"
                    className="form-control"
                    name="name"
                    id="exampleInputtext1"
                    aria-describedby="emailHelp"
                    onChange={(e) => setName(e.target.value)}
                    required
                  />
                </div>
                <div className="col-sm-7 mb-3">
                  <label htmlFor="exampleInputEmail1" className="form-label">
                    Address
                  </label>
                  <input
                    type="text"
                    name="address"
                    className="form-control"
                    id="exampleInputEmail1"
                    aria-describedby="emailHelp"
                    onChange={(e) => setAddress(e.target.value)}
                    required
                  />
                </div>
                <div className="col-sm-7 mb-3">
                  <label htmlFor="exampleInputEmail1" className="form-label">
                    Phone Number
                  </label>
                  <input
                    type="text"
                    className="form-control"
                    name="phone"
                    id="exampleInputEmail1"
                    aria-describedby="emailHelp"
                    onChange={(e) => setPhone(e.target.value)}
                    required
                  />
                </div>
                <div className="col-sm-7 mb-3">
                  <label htmlFor="exampleInputPassword1" className="form-label">
                    Password
                  </label>
                  <input
                    type="password"
                    name="password"
                    className="form-control"
                    id="exampleInputPassword1"
                    onChange={(e) => setPassword(e.target.value)}
                    required
                  />
                  {smallError && (
                    <div id="emailHelp" className="form-text">
                      <span className="text-sm text-danger">
                        password min 6 character!
                      </span>
                    </div>
                  )}
                </div>

                <div className="row">
                  <div className="col-md-5">
                    <button type="submit" className="btn btn-primary">
                      Register
                    </button>
                  </div>
                  <div className="col-md-2 ms-5 my-3">
                    <a
                      href="/login-user"
                      className="me-2 "
                      style={{ textDecoration: "none" }}
                    >
                      {" "}
                      Sign in
                    </a>
                  </div>
                </div>
              </form>
              <div className="row"></div>
            </div>
          </div>
          <div className="col-md-6">
            <img
              src={`${process.env.PUBLIC_URL}/assets/images/login-image.jpg`}
              alt=""
              style={{ height: "750px", width: "747px" }}
            />
          </div>
        </div>
      </div>
      <footer>
        <div className="row">
          <div className="col-12 bg-primary">
            <p className="text-center" id="date">
              &copy;The Pallete-2022
            </p>
          </div>
        </div>
      </footer>
    </div>
  );
}

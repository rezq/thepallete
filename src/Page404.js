import React from 'react';
import Nav from './components/Nav';
import './Page404.css';

export default function Page404() {
  return (
    <div>
      <Nav />
      <div className="container">
        <div className="row justify-content-center">
          <img
            src={'/assets/images/26.-Page-Not-Found.png'}
            alt="Not Found image"
            style={{ width: '50%' }}
          />
          <div className="col-md-12">
            <h4 className="text-center text-black">Oopss....!!</h4>
          </div>

          {/* <h2>Not found</h2> */}
        </div>
      </div>
    </div>
  );
}

import axios from "axios";
import React from "react";
import { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import { NavLink, Link } from "react-router-dom";
import Nav from "./components/Nav";
import WAbutton from "./components/WAbutton";

export default function Paid() {
  let { Id } = useParams("");
  const [order, setOrder] = useState([]);

  useEffect(() => {
    axios
      .get("https://thepallete.site/api/order-paid/" + Id)
      .then((res) => {
        console.log(res.data.data);
        setOrder(res.data.data);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);

  const handleSelesai = (id) => {
    if (window.confirm("Yakin ingin menyelesaikan pesanan ini?")) {
      axios
        .get("https://thepallete.site/api/customer-finish/" + id)
        .then((res) => {
          console.log(res.data);
        });
    } else {
      return false;
    }
  };
  return (
    <div>
      <Nav />
      <div className="container mt-5">
        <div className="row justify-content-center">
          <div className="col-md-3 my-tabs">
            <ul className="nav flex-column w-50">
              <li className="nav-item mt-2">
                <NavLink
                  to={"/my-order/" + Id}
                  className={({ isActive }) =>
                    isActive
                      ? "nav-link bg-primary rounded-3 text-light active "
                      : "nav-link inactive"
                  }
                  aria-current="page"
                >
                  my order
                </NavLink>
              </li>
              <li className="nav-item">
                <NavLink
                  to={"/paid-order/" + Id}
                  className={({ isActive }) =>
                    isActive
                      ? "nav-link bg-primary rounded-3 text-light active "
                      : "nav-link inactive"
                  }
                >
                  On Progress
                </NavLink>
              </li>
              <li className="nav-item mb-2">
                <NavLink
                  to={"/finish-order/" + Id}
                  className={({ isActive }) =>
                    isActive
                      ? "nav-link bg-primary rounded-3 text-light active "
                      : "nav-link inactive"
                  }
                  activestyle="active"
                >
                  Finish
                </NavLink>
              </li>
            </ul>
          </div>
          <div className="col-md-8  ms-3 p-0 my-contents">
            <table className="table">
              <thead>
                <tr className="text-center">
                  <th scope="col">Product</th>
                  <th scope="col">Product Name / invoice</th>

                  <th scope="col">Jumlah</th>
                  <th scope="col">Harga</th>
                  <th scope="col">Total</th>
                  <th scope="col">Status</th>
                  <th scope="col">Action</th>
                </tr>
              </thead>
              <tbody>
                {/* <tr>
                  <th scope="row">1</th>
                  <td></td>
                  <td>Otto</td>
                  <td>@mdo</td>
                </tr> */}
                {order.map((data, index) => (
                  <tr key={index} className="text-center">
                    <td className="text-center">
                      <img
                        src={`https://thepallete.site/images/${data.product.img_thumb}`}
                        className="img-thumbnail"
                        style={{ width: "80px", maxHeight: "80px" }}
                        alt="..."
                      />
                    </td>
                    <td>
                      <p className="border-bottom border-1">
                        {data.product.nama}
                      </p>
                      <span className="badge bg-light text-dark shadow">
                        {data.invoice}
                      </span>
                    </td>
                    <td>
                      <p className="border-bottom border-1"> {data.qty}</p>
                    </td>
                    <td className="text-center">
                      <p className="border-bottom border-1">
                        Rp.{data.price.toLocaleString()}
                      </p>
                    </td>
                    <td>
                      <p className="border-bottom border-1">
                        Rp.{data.total.toLocaleString()}
                      </p>
                    </td>
                    <td>
                      {data.status_order == "PAID" ? (
                        <span className="badge bg-primary">
                          {" "}
                          {data.status_order}
                        </span>
                      ) : (
                        <span className="badge bg-success">
                          {" "}
                          {data.status_order}
                        </span>
                      )}
                    </td>
                    {data.status_order == "SEND" ? (
                      <td>
                        <button
                          type="button"
                          className="btn btn-outline-primary btn-sm"
                          onClick={(e) => handleSelesai(data.id)}
                        >
                          Click Selesai
                        </button>
                        <p id="demo"></p>
                      </td>
                    ) : (
                      <td>
                        <button
                          type="button"
                          className="btn btn-outline-primary btn-sm disabled"
                        >
                          Click Selesai
                        </button>
                      </td>
                    )}
                  </tr>
                ))}
              </tbody>
            </table>
          </div>
        </div>
        <WAbutton />
      </div>
    </div>
  );
}

import axios from "axios";
import React, { useEffect, useState } from "react";
import { Link, useNavigate, useParams } from "react-router-dom";
import Nav from "./components/Nav";
import OtherProduct from "./components/OtherProduct";
import "./ProductDetail.css";

export default function ProductDetail(props) {
  //geet all props from parent
  const { productList } = props;
  // definined id for url params by id item
  let { Id } = useParams("");
  // variable for redirect page payment
  const navigate = useNavigate();
  // get data from localStorage
  const [myLocalStorage, setMyLocalStorage] = useState({});

  //set data item to hooks
  const [count, setCount] = useState(1);
  const [ongkir, setOngkir] = useState(30000);
  const [nama, setNama] = useState("");
  const [userID, setUserID] = useState(myLocalStorage.id);
  const [nameUser, setNameUser] = useState("");
  const [address, setAddress] = useState("");
  const [phone, setPhone] = useState("");
  const [email, setEmail] = useState("");

  const [price, setPrice] = useState("");
  const [status, setStatus] = useState("");
  const [desc, setDesc] = useState("");
  const [category, setCategory] = useState("");
  const [imgThumb, setImgThumb] = useState("");
  const [img1, setImage1] = useState("");
  const [img2, setImage2] = useState("");
  const [note, setNote] = useState("mantap");
  const [total, setTotal] = useState(0);
  const data = localStorage.getItem("user-info");
  useEffect(() => {
    detailProduct();
    // getFromStorage();

    window.scrollTo(0, 0);
  }, [Id]);

  // get data from localStorage after user - login
  useEffect(() => {
    if (data) {
      setMyLocalStorage(JSON.parse(data));
      setUserID(myLocalStorage.id);
      setEmail(myLocalStorage.email);
      setNameUser(myLocalStorage.name);
      setAddress(myLocalStorage.address);
      setPhone(myLocalStorage.phone);
    }
  }, [data, userID, nameUser, email, address, phone]);
  // const getFromStorage = () => {};

  /**
   * Display the specified resource.
   *
   * @param  int  $id from product
   * @return \Illuminate\Http\Response
   */
  const detailProduct = async () => {
    let res = await axios.get(
      "https://thepallete.site/api/detail/" + Id + "/product"
    );

    let item = await res.data.data;
    setNama(item.nama);
    setPrice(item.price);
    setDesc(item.desc);
    setStatus(item.status.nama);
    setCategory(item.category.nama);
    setImgThumb(item.img_thumb);
    setImage1(item.img1);
    setImage2(item.img2);

    // .then((res) => {
    //   // console.warn(res.data.data);
    //   setNama(res.data.data.nama);
    //   setPrice(res.data.data.price);
    //   setDesc(res.data.data.desc);
    //   setStatus(res.data.data.status.nama);
    //   setCategory(res.data.data.category.nama);
    //   setImgThumb(res.data.data.img_thumb);
    //   setImage1(res.data.data.img1);
    //   setImage2(res.data.data.img2);
  };
  const buyItem = {
    user_id: userID,
    product_id: Id,
    qty: count,
    price: price,
    note: note,
  };
  const storeData = () => {
    console.warn(buyItem);
    axios
      .post("https://thepallete.site/api/buy-item", buyItem)
      .then((res) => {
        console.log(res);
        // navigate('/success-payment');
        window.location.href = "/success-payment";
      })
      .catch((err) => {
        console.log(err);
      });
  };

  const handleMinus = (e) => {
    if (count > 1) {
      setCount(count - 1);
      setTotal(total - price);
    }
  };
  const handlePlus = (e) => {
    setCount(count + 1);
    setTotal(price * count + price);
  };
  return (
    <div>
      <Nav />
      <div className="container my-5">
        <div className="wrapper">
          <div className="row justify-content-between ">
            <div className="col-md-5 ms-4 mt-3">
              <img
                src={`https://thepallete.site/images/${imgThumb}`}
                alt="Thumbnail"
                style={{ width: "400px", height: "300px" }}
                // onError={(e) => (e.target.style.display = 'none')}
              />
              <div className="row d-flex justify-content-start mt-4">
                <div className="col-md-12 d-flex">
                  {img1 == null ? (
                    ""
                  ) : (
                    <div className="my-image ms-3">
                      <img
                        src={
                          process.env.PUBLIC_URL +
                          `https://thepallete.site/images/${img1}`
                        }
                        alt="img1"
                        style={{ width: "100px", height: "80px" }}
                        // onError={(e) => (e.target.style.display = 'none')}
                      />
                    </div>
                  )}

                  {img2 == null ? (
                    ""
                  ) : (
                    <div className="my-image ms-3">
                      <img
                        src={`https://thepallete.site/images/${img2}`}
                        alt="img2"
                        style={{ width: "100px", height: "80px" }}
                        // onError={(e) => (e.target.style.display = 'none')}
                      />
                    </div>
                  )}
                </div>
              </div>
            </div>
            <div className="col-md-3 mt-4">
              <h3 className="">{nama}</h3>
              <hr />
              <div className="my-status">
                {status == "Habis" ? (
                  <span className="badge bg-danger">Habis</span>
                ) : status == "Ready" ? (
                  <span className="badge bg-success">Ready</span>
                ) : status == "Pre-Order" ? (
                  <span className="badge bg-warning">Pre-Order</span>
                ) : (
                  ""
                )}
              </div>

              <h2>{price ? "Rp." + price.toLocaleString() : ""}</h2>

              <h6>{desc}</h6>
              <div className="row mt-5">
                <div className="col-12 text-end">
                  <span className="badge rounded-pill bg-dark ">
                    #{category}
                  </span>
                </div>
              </div>
            </div>
            <div className="col-md-3 mt-4">
              <div className="box-card">
                <div className="row">
                  <div className="ads text-center">
                    <h5>Langsung Check out</h5>
                  </div>
                </div>
                <div className="row">
                  <div className="ceckout ">
                    <p className="card-text mt-1">{nama}</p>
                    <div className="row d-flex justify-content-between ">
                      <div className="col-6">
                        <p>Harga </p>
                        <p className="mt-1">Rp.{price.toLocaleString()}</p>
                      </div>
                      <div className="col-6 ">
                        <div className="nilaiAwal text-end">
                          <p>Jumlah</p>
                          <p className="nilai mt-1">x {count}</p>
                        </div>
                      </div>
                    </div>
                    <hr />
                    <div className="row">
                      <div className="col-md-6 text-start">
                        <h5>Total :</h5>
                      </div>
                      <div className="col-6 text-end">
                        <h5>
                          {total.toLocaleString() == 0
                            ? price.toLocaleString()
                            : total.toLocaleString()}
                        </h5>
                      </div>
                    </div>

                    <div className="counter d-flex justify-content-center align-items-center mb-2">
                      <button
                        type="button"
                        className="minus"
                        onClick={handleMinus}
                      >
                        -
                      </button>
                      <input
                        type="text"
                        min={1}
                        max={2000}
                        placeholder="1"
                        // onChange={handleTotal}
                        value={count}
                      />

                      <button
                        type="button"
                        className="plus"
                        onClick={handlePlus}
                      >
                        +
                      </button>
                    </div>
                    {!data && (
                      <div className="row">
                        <div className="d-grid gap-2">
                          <a href={"/login-user"} className="btn btn-primary">
                            Sign-in Now
                          </a>
                        </div>
                      </div>
                    )}
                    {data && (
                      <div className="row">
                        <div className="d-grid gap-2">
                          <button
                            className="btn btn-primary"
                            type="button"
                            data-bs-toggle="modal"
                            data-bs-target="#exampleModal"
                          >
                            Beli
                          </button>
                        </div>
                      </div>
                    )}
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <OtherProduct productList={productList} />

      {/* Modal Cart */}
      <div
        className="modal fade"
        id="exampleModal"
        tabIndex="-1"
        aria-labelledby="exampleModalLabel"
        aria-hidden="true"
      >
        <div className="modal-dialog">
          <div className="modal-content">
            <div className="modal-header">
              <h5 className="modal-title" id="exampleModalLabel">
                Belanja Anda
              </h5>
              <button
                type="button"
                className="btn-close"
                data-bs-dismiss="modal"
                aria-label="Close"
              ></button>
            </div>
            <div className="modal-body">
              <div className="row justify-content-center">
                <div className="col-md-5">
                  <img
                    src={`https://thepallete.site/images/${imgThumb}`}
                    alt="Thumbnail"
                    style={{ width: "80%", maxHeight: "150px" }}
                    // onError={(e) => (e.target.style.display = 'none')}
                  />
                </div>
                <div className="col-md-7">
                  <div className="text-left">
                    <h6>{nama}</h6>
                  </div>
                  <div className="row justify-content-between">
                    <div className="col-6">
                      <p>Harga</p>
                      <p>Qty</p>
                    </div>
                    <div className="col-6 text-end">
                      <p className="text-right">Rp.{price.toLocaleString()}</p>
                      <p className="text-right">x {count}</p>
                    </div>
                  </div>
                  <hr />
                  <div className="row text-danger text-end">
                    <h5>
                      Total :{" "}
                      {total.toLocaleString() == 0
                        ? price.toLocaleString()
                        : total.toLocaleString()}
                    </h5>
                  </div>
                </div>
                <div className="row  text-end">
                  <div className="form-floating">
                    <textarea
                      className="form-control"
                      placeholder="Leave a comment here"
                      id="floatingTextarea2"
                      style={{ height: "100px" }}
                      onChange={(e) => setNote(e.target.value)}
                    ></textarea>
                    <label htmlFor="floatingTextarea2 text-sm">
                      Catatan Untuk Pengirim / Alamat Tujuan
                    </label>
                  </div>
                </div>
              </div>
            </div>
            <div className="modal-footer">
              <button
                type="button"
                className="btn btn-secondary"
                data-bs-dismiss="modal"
              >
                Close
              </button>

              <div className="d-grid gap-2">
                {/* <form onSubmit={storeData}>
                  {' '} */}
                <button
                  className="btn btn-primary"
                  type="button"
                  onClick={storeData}
                >
                  CheckOut{" "}
                </button>
                {/* </form> */}
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

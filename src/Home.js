import React from 'react';
import CardProduct from './components/CardProduct';
import Carousel from './components/Carousel';
import CategoryBadge from './components/CategoryBadge';
import Footer from './components/Footer';
import Nav from './components/Nav';
import OtherProduct from './components/OtherProduct';
import WAbutton from './components/WAbutton';
export default function Home(props) {
  const { productList } = props;
  return (
    <div>
      <Nav />
      <div className="container">
        <div className="row">
          <Carousel />
          <CategoryBadge />
          <CardProduct productList={productList} />

          <OtherProduct productList={productList} />
        </div>
        <WAbutton />
      </div>
      <Footer />
    </div>
  );
}

import React from "react";
import "./Carousel.css";
import axios from "axios";
import { Link } from "react-router-dom";
import { useState, useEffect } from "react";

export default function CardProduct(props) {
  const { productList } = props;
  const [product, setProduct] = useState([]);
  // console.warn(productList);

  return (
    <div className="container">
      <div className="row my-5">
        <div className="col-md-6">
          <h5>The Pallete Product</h5>
        </div>
        <div className="col-md-6 text-end">
          <a href={"/all-product"} style={{ textDecoration: "none" }}>
            <span className="badge rounded-pill bg-dark">Lainnya</span>
          </a>
        </div>
        <hr />
      </div>
      <div className="row ">
        {productList.map((product, index) => (
          <div className="col-sm-2 my-3" key={product.id}>
            <div className="card">
              <img
                src={`https://thepallete.site//images/${product.img_thumb}`}
                className="card-img-top"
                style={{ height: "180px" }}
                alt="..."
                onError={(e) => (e.target.style.display = "none")}
              />

              <div className="card-body" style={{ height: "120px" }}>
                {product.status_id == 1 ? (
                  <span className="badge bg-danger">Habis</span>
                ) : (
                  ""
                )}
                {product.status_id == 2 ? (
                  <span className="badge bg-success">Ready</span>
                ) : (
                  ""
                )}
                {product.status_id == 3 ? (
                  <span className="badge bg-warning">Pre-Order</span>
                ) : (
                  ""
                )}
                <div className="row mt-2">
                  <div className="col-md-12">
                    <h6 className="card-title">{product.nama} </h6>
                  </div>
                </div>

                <h6 className="card-title mb-3">
                  Rp.{product.price.toLocaleString()}
                </h6>
                {/* <p className="card-text mt-">
                  {product.desc.slice(0, 20) + '...'}
                </p> */}
                <div className="row d-flex justify-content-center"></div>
              </div>

              <Link to={"/detail/" + product.id + "/product"}>
                <div className="d-grid gap-2">
                  <button className="btn btn-primary" type="button">
                    Lihat{" "}
                  </button>
                </div>
              </Link>
            </div>
          </div>
        ))}
      </div>
    </div>
  );
}

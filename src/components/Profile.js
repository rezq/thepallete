import React from "react";
import Nav from "./Nav";
import { Link, NavLink, useParams } from "react-router-dom";
import { useState, useEffect } from "react";
import axios from "axios";
import WAbutton from "./WAbutton";
export default function Profile() {
  let { Id } = useParams("");
  const [message, setMessage] = useState(false);
  const [myLocalStorage, setMyLocalStorage] = useState({});
  const [email, setEmail] = useState("");
  const [name, setName] = useState("");
  const [phone, setPhone] = useState("");
  const [address, setAddress] = useState("");
  const [userID, setUserID] = useState();
  const [userData, setUserData] = useState([]);
  const data = localStorage.getItem("user-info");

  useEffect(() => {
    getData();
  }, []);

  const getData = () => {
    axios
      .get("https://thepallete.site/api/customer-profile/" + Id)
      .then((res) => {
        setUserData(res.data.data);
        console.log(userData);
      })
      .catch((err) => {
        console.log(err);
      });
  };
  const body = {
    email: email,
    name: name,
    address: address,
    phone: phone,
  };
  const handleProfile = (e) => {
    e.preventDefault();

    console.log(Id, body);
    // const formData = new FormData();
    // formData.append('name', name);
    // formData.append('email', email);
    // formData.append('phone', phone);
    // formData.append('address', address);

    axios
      .post("https://thepallete.site/api/customer-profile/" + Id, body)
      .then((res) => {
        console.log(res.data.data);
        setMessage(true);
      })
      .catch((err) => {
        console.log(err);
      });
  };
  return (
    <div>
      <Nav />
      <div className="container mt-5">
        <div className="row justify-content-center">
          <div className="col-md-3 my-tabs">
            <ul className="nav flex-column w-50">
              <li className="nav-item mt-2">
                <NavLink
                  to={"/profile/" + userData.id}
                  className={({ isActive }) =>
                    isActive
                      ? "nav-link bg-primary rounded-3 text-light active "
                      : "nav-link inactive"
                  }
                  aria-current="page"
                  activestyle="active"
                >
                  Profile
                </NavLink>
              </li>
              <li className="nav-item mb-2">
                <NavLink
                  to={"/change-password/" + userData.id}
                  className={({ isActive }) =>
                    isActive
                      ? "nav-link bg-primary rounded-3 text-light active "
                      : "nav-link inactive"
                  }
                >
                  Change&nbsp;Password
                </NavLink>
              </li>
            </ul>
          </div>
          <div className="col-md-8  ms-3 p-0 my-contents">
            {message && (
              <div className="alert alert-primary" role="alert">
                Profile berhasil di update..!
              </div>
            )}

            <form className="col-md-10 ms-3 my-3" onSubmit={handleProfile}>
              <div className="mb-3">
                <label htmlFor="exampleInputEmail1" className="form-label">
                  Email address
                </label>
                <input
                  type="email"
                  name="email"
                  className="form-control"
                  id="exampleInputEmail1"
                  defaultValue={userData.email}
                  onChange={(e) => setEmail(e.target.value)}
                />
                <div id="emailHelp" className="form-text">
                  We'll never share your email with anyone else.
                </div>
              </div>
              <div className="mb-3">
                <label htmlFor="exampleInputPassword1" className="form-label">
                  Full Name
                </label>
                <input
                  type="text"
                  name="name"
                  className="form-control"
                  id="exampleInputPassword1"
                  defaultValue={userData.name}
                  onChange={(e) => setName(e.target.value)}
                />
              </div>
              <div className="mb-3">
                <label htmlFor="exampleInputEmail1" className="form-label">
                  Phone
                </label>
                <input
                  type="text"
                  name="phone"
                  className="form-control"
                  id="exampleInputEmail1"
                  defaultValue={userData.phone}
                  onChange={(e) => setPhone(e.target.value)}
                />
              </div>
              <div className="mb-3">
                <label
                  htmlFor="exampleFormControlTextarea1"
                  className="form-label"
                >
                  Address
                </label>
                <textarea
                  className="form-control"
                  id="exampleFormControlTextarea1"
                  rows="3"
                  name="address"
                  defaultValue={userData.address}
                  onChange={(e) => setAddress(e.target.value)}
                ></textarea>
              </div>

              <button type="submit" className="btn btn-primary">
                Submit
              </button>
            </form>
          </div>
        </div>
        <WAbutton />
      </div>
    </div>
  );
}

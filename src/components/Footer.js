import React from "react";
import "./Footer.css";
import "./Utilities.css";
export default function Footer() {
  return (
    <div>
      <footer>
        <div class="container">
          <div class="row">
            <div class="col-lg-4 text-lg-start text-center">
              <a href="" class="mb-30">
                <img
                  src={`${process.env.PUBLIC_URL}/assets/furniture.png`}
                  alt=""
                  style={{ width: "20%" }}
                />
              </a>
              <p class="mt-30 text-lg color-palette-1 mb-30">
                The Pallete membantu customer
                <br /> untuk memudahkan berbelanja furniture
              </p>
              <p class="mt-30 text-lg color-palette-1 mb-30">
                Copyright 2022. All Rights Reserved.
              </p>
            </div>
            <div class="col-lg-8 mt-lg-0 mt-20">
              <div class="row gap-sm-0">
                <div class="col-md-4 col-6 mb-lg-0 mb-25">
                  <p class="text-lg fw-semibold color-palette-1 mb-12">
                    Company
                  </p>
                  <ul class="list-unstyled">
                    <li class="mb-6">
                      <a
                        href=""
                        class="text-lg color-palette-1 text-decoration-none"
                      >
                        About Us
                      </a>
                    </li>
                    <li class="mb-6">
                      <a
                        href=""
                        class="text-lg color-palette-1 text-decoration-none"
                      >
                        Press Release
                      </a>
                    </li>
                    <li class="mb-6">
                      <a
                        href=""
                        class="text-lg color-palette-1 text-decoration-none"
                      >
                        Terms of Use
                      </a>
                    </li>
                    <li class="mb-6">
                      <a
                        href=""
                        class="text-lg color-palette-1 text-decoration-none"
                      >
                        Privacy & Policy
                      </a>
                    </li>
                  </ul>
                </div>
                <div class="col-md-4 col-6 mb-lg-0 mb-25">
                  <p class="text-lg fw-semibold color-palette-1 mb-12">
                    Support
                  </p>
                  <ul class="list-unstyled">
                    <li class="mb-6">
                      <a
                        href=""
                        class="text-lg color-palette-1 text-decoration-none"
                      >
                        Refund Policy
                      </a>
                    </li>
                    <li class="mb-6">
                      <a
                        href=""
                        class="text-lg color-palette-1 text-decoration-none"
                      >
                        Customer Care
                      </a>
                    </li>
                    <li class="mb-6">
                      <a
                        href=""
                        class="text-lg color-palette-1 text-decoration-none"
                      >
                        Join Mitra
                      </a>
                    </li>
                  </ul>
                </div>
                <div class="col-md-4 col-12 mt-lg-0 mt-md-0 mt-25">
                  <p class="text-lg fw-semibold color-palette-1 mb-12">
                    Connect
                  </p>
                  <ul class="list-unstyled">
                    <li class="mb-6">
                      <a
                        href="mailto: hi@store.gg"
                        class="text-lg color-palette-1 text-decoration-none"
                      >
                        hi@thepalete.com
                      </a>
                    </li>
                    <li class="mb-6">
                      <a
                        href="mailto: team@store.gg"
                        class="text-lg color-palette-1 text-decoration-none"
                      >
                        team@thepallete.com
                      </a>
                    </li>
                    <li class="mb-6">
                      <a
                        href="http://maps.google.com/?q=Pasific 12,
                                        Jakarta Selatan"
                        class="text-lg color-palette-1 text-decoration-none"
                      >
                        SouthEast Asia, Jakarta Selatan
                      </a>
                    </li>
                    <li class="mb-6">
                      <a
                        href="tel: 02111229090"
                        class="text-lg color-palette-1 text-decoration-none"
                      >
                        021 - 1122 - 0000
                      </a>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
      </footer>
    </div>
  );
}

import React from "react";
import { useState, useEffect } from "react";
import axios from "axios";
import { Link } from "react-router-dom";
export default function OtherProduct() {
  const [productList, setProduct] = useState([]);

  useEffect(() => {
    getProduct();
    // return () => {
    //   cleanup
    // }
  }, []);

  const getProduct = () => {
    axios
      .get("https://thepallete.site/api/products-lainnya")
      .then((res) => {
        // console.log(res.data.data);
        setProduct(res.data.data);
      })
      .catch((err) => {
        console.log(err);
      });
  };
  return (
    <div className="container">
      <div className="row my-5">
        <div className="col-md-6">
          <h5>Product Lainnya</h5>
        </div>
        <div className="col-md-6 text-end">
          <a href={"/all-product"} style={{ textDecoration: "none" }}>
            <span className="badge rounded-pill bg-dark">Lainnya</span>
          </a>
        </div>
        <hr />
      </div>
      <div className="row my-5">
        {productList.map((product, index) => (
          <div className="col-md-2 my-3" key={product.id}>
            <div
              className="card "
              style={{ boxShadow: "0px 2px 15px rgba(7, 13, 65, 0.3)" }}
            >
              <img
                src={`https://thepallete.site/images/${product.img_thumb}`}
                className="card-img-top"
                style={{ height: "180px" }}
                alt="..."
                onError={(e) => (e.target.style.display = "none")}
              />
              <div className="card-body" style={{ height: "120px" }}>
                {product.status_id == 1 ? (
                  <span className="badge bg-danger">Habis</span>
                ) : (
                  ""
                )}
                {product.status_id == 2 ? (
                  <span className="badge bg-success">Ready</span>
                ) : (
                  ""
                )}
                {product.status_id == 3 ? (
                  <span className="badge bg-warning">Pre-Order</span>
                ) : (
                  ""
                )}
                <div className="row mt-2">
                  <div className="col-md-12">
                    <h6 className="card-title">{product.nama} </h6>
                  </div>
                  <div className="col"></div>
                </div>

                <h6 className="card-title mb-4">
                  Rp.{product.price.toLocaleString()}
                </h6>
                {/* <p className="card-text mt-2">
                  {product.desc.slice(0, 30) + '...'}
                </p> */}
                <div className="row d-flex justify-content-center"></div>
              </div>
              <a href={"/detail/" + product.id + "/product"}>
                <div className="d-grid gap-2">
                  <button className="btn btn-primary" type="button">
                    Lihat{" "}
                  </button>
                </div>
              </a>
            </div>
          </div>
        ))}
      </div>
    </div>
  );
}

import React from 'react';
import './WAbutton.css';

export default function WAbutton() {
  const nama = 'format%20data%0Anama%20lengkap%20:%0Aemail%20:';

  return (
    <div>
      <div className=" row levitation">
        <div className="text-end">
          <a
            href="https://api.whatsapp.com/send?phone=6287832186926&text=Saya%20tertarik%20untuk%20membeli%20produk%20ini%20segera%20masukkan%20format%20data%0Anama%20lengkap%20:%0Aemail%20:"
            className="btn btn-outline-success rounded-4"
            target={'_blank'}
            rel="noreferrer"
          >
            <div className="row rounded-3" style={{ maxHeight: '40px' }}>
              <div className="col-md-3">
                <img
                  src="/whatsapp.png"
                  alt=""
                  style={{ width: '40px' }}
                  className="rounded-4"
                />{' '}
              </div>
            </div>
          </a>
        </div>
      </div>
    </div>
  );
}

import React from 'react';
import './Carousel.css';

export default function Carousel() {
  return (
    <div>
      <div className="container my-4">
        <div
          id="carouselExampleCaptions"
          className="carousel slide"
          data-bs-ride="carousel"
        >
          <div className="carousel-indicators ">
            <button
              type="button"
              data-bs-target="#carouselExampleCaptions"
              data-bs-slide-to="0"
              className="active "
              aria-current="true"
              aria-label="Slide 1"
            ></button>
            <button
              type="button"
              data-bs-target="#carouselExampleCaptions"
              data-bs-slide-to="1"
              aria-label="Slide 2"
            ></button>
            <button
              type="button"
              data-bs-target="#carouselExampleCaptions"
              data-bs-slide-to="2"
              aria-label="Slide 3"
            ></button>
          </div>
          <div className="carousel-inner">
            <div className="carousel-item active">
              <img
                src={`${process.env.PUBLIC_URL}/assets/images/big-flat.jpg`}
                className="d-block w-100"
                alt="..."
              />
              <div className="carousel-caption d-none d-md-block">
                {/* <h5>Belanja Sekarang juga</h5>
                <p>Temukan kebutuhan kalian</p> */}
              </div>
            </div>
            <div className="carousel-item">
              <img
                src={`${process.env.PUBLIC_URL}/assets/images/sale50.jpg`}
                className="d-block w-100"
                alt="..."
              />
              <div className="carousel-caption d-none d-md-block">
                {/* <h5>segera checkout dan hubungi admin</h5>
                <p>Dan dapatkan info discount lebih lanjut</p> */}
              </div>
            </div>
            <div className="carousel-item">
              <img
                src={`${process.env.PUBLIC_URL}/assets/images/sale502.jpg`}
                className="d-block w-100"
                alt="..."
              />
              <div className="carousel-caption d-none d-md-block">
                {/* <h5>Jangan sampai kehabisan!!</h5>
                <p>
                  Some representative placeholder content for the third slide.
                </p> */}
              </div>
            </div>
          </div>
          <button
            className="carousel-control-prev"
            type="button"
            data-bs-target="#carouselExampleCaptions"
            data-bs-slide="prev"
          >
            <span
              className="carousel-control-prev-icon"
              aria-hidden="true"
            ></span>
            <span className="visually-hidden">Previous</span>
          </button>
          <button
            className="carousel-control-next"
            type="button"
            data-bs-target="#carouselExampleCaptions"
            data-bs-slide="next"
          >
            <span
              className="carousel-control-next-icon"
              aria-hidden="true"
            ></span>
            <span className="visually-hidden">Next</span>
          </button>
        </div>
      </div>
    </div>
  );
}

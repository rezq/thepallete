import React from "react";
import Nav from "./Nav";
import { Link, NavLink, useParams } from "react-router-dom";
import { useState, useEffect } from "react";
import axios from "axios";
import WAbutton from "./WAbutton";
export default function ChangePass() {
  let { Id } = useParams("");

  const [passError, setPassErr] = useState(false);
  const [smallError, setSmallError] = useState(false);
  const [message, setMessage] = useState(false);
  const [password, setPassword] = useState("");
  const [confirmPassword, setConfirmPassword] = useState("");
  const [myLocalStorage, setMyLocalStorage] = useState({});
  const [email, setEmail] = useState("");
  const [name, setName] = useState("");
  const [phone, setPhone] = useState("");
  const [address, setAddress] = useState("");
  const [userID, setUserID] = useState();
  const [userData, setUserData] = useState([]);
  const data = localStorage.getItem("user-info");

  useEffect(() => {
    getData();
  }, [password, confirmPassword]);

  const getData = () => {
    axios
      .get("http://127.0.0.1:8000/api/customer-profile/" + Id)
      .then((res) => {
        setUserData(res.data.data);
        console.log(userData);
      })
      .catch((err) => {
        console.log(err);
      });
  };
  const body = {
    password: password,
  };
  const handlePassword = (e) => {
    e.preventDefault();
    if (password.length < 6) {
      setSmallError(true);
      console.log("stop small");
      return false;
    }
    if (confirmPassword !== password) {
      setPassErr(true);
      console.log("stop match");
      return false;
    } else {
      axios
        .post("https://thepallete.site/api/change-pass/" + Id, body)
        .then((res) => {
          console.log(res.data.data);
          setMessage(true);
          setSmallError(false);
          setPassErr(false);
        })
        .catch((err) => {
          console.log(err);
        });
      setPassword("");
      setConfirmPassword("");
    }

    console.log(Id, body);
    // const formData = new FormData();
    // formData.append('name', name);
    // formData.append('email', email);
    // formData.append('phone', phone);
    // formData.append('address', address);
  };
  return (
    <div>
      <Nav />
      <div className="container mt-5">
        <div className="row justify-content-center">
          <div className="col-md-3 my-tabs">
            <ul className="nav flex-column w-50">
              <li className="nav-item mt-2">
                <NavLink
                  to={"/profile/" + userData.id}
                  className={({ isActive }) =>
                    isActive
                      ? "nav-link bg-primary rounded-3 text-light active "
                      : "nav-link inactive"
                  }
                  aria-current="page"
                >
                  Profile
                </NavLink>
              </li>
              <li className="nav-item mb-2">
                <NavLink
                  to={"/change-password/" + userData.id}
                  className={({ isActive }) =>
                    isActive
                      ? "nav-link bg-primary rounded-3 text-light active "
                      : "nav-link inactive"
                  }
                >
                  Change&nbsp;Password
                </NavLink>
              </li>
            </ul>
          </div>
          <div className="col-md-8  ms-3 p-0 my-contents">
            {passError && (
              <div className="alert alert-danger" role="alert">
                Password doesnt match..!
              </div>
            )}

            {message && (
              <div className="alert alert-primary" role="alert">
                Password Berhasil di update..!
              </div>
            )}

            <form className="col-md-10 ms-3 my-3" onSubmit={handlePassword}>
              <div className="mb-3">
                <label htmlFor="exampleInputEmail1" className="form-label">
                  Password
                </label>
                <input
                  type="password"
                  name="password"
                  className="form-control"
                  id="exampleInputEmail1"
                  onChange={(e) => setPassword(e.target.value)}
                />
                {smallError && (
                  <div id="emailHelp" className="form-text">
                    <span className="text-sm text-danger">
                      password min 6 character!
                    </span>
                  </div>
                )}
              </div>
              <div className="mb-3">
                <label htmlFor="exampleInputPassword1" className="form-label">
                  Confirm Password
                </label>
                <input
                  type="password"
                  name="confirm_password"
                  className="form-control"
                  id="exampleInputPassword1"
                  onChange={(e) => setConfirmPassword(e.target.value)}
                />
              </div>

              <button type="submit" className="btn btn-primary">
                Submit
              </button>
            </form>
          </div>
        </div>
        <WAbutton />
      </div>
    </div>
  );
}

import React from 'react';

export default function Type() {
  return (
    <div>
      <div className="container mt-0">
        <div className="row mt-0">
          <div className="col-12 ps-0">
            <div className="text-center">
              <img
                src="/assets/images/type.png"
                alt=""
                style={{ width: '40%' }}
              />
              <h3
                className=" text-lg text-center"
                // style={{ marginTop: '100px' }}
              >
                Kamu belum mengetikkan apapun...
              </h3>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

import React from 'react';
import { Link, NavLink } from 'react-router-dom';
import { useState, useEffect } from 'react';
import gravatar from 'gravatar/lib/gravatar';

export default function Nav() {
  let ava = gravatar;

  const [myLocalStorage, setMyLocalStorage] = useState({});
  const [email, setEmail] = useState('');
  const [name, setName] = useState('');
  const [phone, setPhone] = useState('');
  const [address, setAddress] = useState('');
  const [userID, setUserID] = useState();
  const data = localStorage.getItem('user-info');
  let avatar = ava.url(
    myLocalStorage.email,
    { s: '100', r: 'x', d: 'wavatar' },
    true
  );
  useEffect(() => {
    setMyLocalStorage(JSON.parse(data));
    if (data) {
      setUserID(myLocalStorage.id);
      setEmail(myLocalStorage.email);
      setName(myLocalStorage.name);
      setAddress(myLocalStorage.address);
      setPhone(myLocalStorage.phone);
    }
  }, [data, userID, email, name, phone, address]);

  return (
    <div>
      <header>
        <nav className="navbar navbar-expand-lg navbar-dark bg-primary">
          <div className="container">
            <a className="navbar-brand" href="/">
              The Pallete
            </a>
            <button
              className="navbar-toggler"
              type="button"
              data-bs-toggle="collapse"
              data-bs-target="#navbarNavAltMarkup"
              aria-controls="navbarNavAltMarkup"
              aria-expanded="false"
              aria-label="Toggle navigation"
            >
              <span className="navbar-toggler-icon"></span>
            </button>
            <div className="collapse navbar-collapse" id="navbarNavAltMarkup">
              <div className="navbar-nav me-auto">
                <NavLink
                  to={'/'}
                  className="nav-link"
                  aria-current="page"
                  activestyle="active"
                >
                  Home
                </NavLink>

                {!data ? (
                  ''
                ) : (
                  <NavLink
                    to={'/my-order/' + userID}
                    className="nav-link"
                    activestyle="active"
                  >
                    My Order
                  </NavLink>
                )}
              </div>

              {data ? (
                <div className="">
                  <div className="dropdown">
                    <a
                      href="s"
                      className=""
                      type="button"
                      id="dropdownMenuButton1"
                      data-bs-toggle="dropdown"
                      aria-expanded="false"
                    >
                      <img
                        src={avatar}
                        alt=""
                        style={{ width: '40px' }}
                        className="rounded-circle float-start me-2"
                      />
                      <span className="text-align-center text-light me-2 mt-2">
                        {myLocalStorage.name}
                      </span>
                    </a>
                    <ul
                      className="dropdown-menu"
                      aria-labelledby="dropdownMenuButton1"
                    >
                      <li>
                        <a
                          href={'/profile/' + userID}
                          className="dropdown-item"
                        >
                          Profile
                        </a>
                      </li>
                      <li>
                        <a
                          href={'/login-user'}
                          className="dropdown-item"
                          onClick={(e) => localStorage.removeItem('user-info')}
                        >
                          Sign Out
                        </a>
                      </li>
                    </ul>
                  </div>
                </div>
              ) : (
                <a
                  href={'/login-user'}
                  className="btn btn-sm btn-outline-light"
                >
                  Sign in
                </a>
              )}
            </div>
          </div>
        </nav>
      </header>
    </div>
  );
}

import React from 'react';

export default function InfoOrder() {
  return (
    <div>
      <div className="text-center">
        <img
          src="/assets/images/12.-Ecommerce.png"
          alt=""
          style={{ width: '40%' }}
        />
        <h5 className=" text-lg text-center">
          Daftar Antrian Order Kamu Kosong
        </h5>
        <p className=" text-sm">Segera belanja / Check Daftar Progressmu..</p>
        <div className="my text-center">
          <a
            href="https://api.whatsapp.com/send?phone=6287832186926&text=Saya%20tertarik%20untuk%20membeli%20produk%20ini%20segera"
            className="btn btn-outline-success rounded-2"
          >
            <div className="row" style={{ maxHeight: '40px' }}>
              <div className="col-md-3">
                <img src="/whatsapp.png" alt="" style={{ width: '40px' }} />{' '}
              </div>
              <div className="col-md-7 d-flex  align-items-center">
                <p className="text-start pt-2"> Hubungi&nbsp;Admin</p>
              </div>
            </div>
          </a>
        </div>
      </div>
    </div>
  );
}

import React from 'react';
import { useNavigate } from 'react-router-dom';
import './CategoryBadge.css';

export default function CategoryBadge() {
  const navigate = useNavigate();

  const handleClick = () => {
    navigate('/krawangan');
  };

  const handleLampu = () => {
    navigate('/lampu');
  };
  const handleKantor = () => {
    navigate('/kantor');
  };
  const handleSculpture = () => {
    navigate('/sculpture');
  };
  return (
    <div>
      <div className="container mt-5 ms-0">
        <h5 className="text mb-2">Temukan Product Category mu</h5>
        <hr />
        <div className="row justify-content-center">
          <div className="col-md-3">
            <div className="my-cards size">
              <img
                src="https://images.unsplash.com/photo-1619778466072-57449582a1ac?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1886&q=80"
                className="my-img"
                alt="..."
                onClick={handleClick}
              />

              <div className="my-cards-title">
                <h5 className="">Krawangan</h5>
              </div>
            </div>
          </div>
          <div className="col-md-3">
            <div className="my-cards size">
              <img
                src="https://images.unsplash.com/photo-1530107973768-581951e62d34?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=387&q=80"
                className="my-img"
                alt="..."
                onClick={handleLampu}
              />
              <div className="my-cards-title">
                <h5 className="">Lampu</h5>
              </div>
            </div>
          </div>
          <div className="col-md-3">
            <div className="my-cards size">
              <img
                src="https://images.unsplash.com/photo-1497032628192-86f99bcd76bc?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1170&q=80"
                className="my-img"
                alt="..."
                onClick={handleKantor}
              />
              <div className="my-cards-title">
                <h6 className="">
                  Perlengkapan
                  <br />
                  kantor
                </h6>
              </div>
            </div>
          </div>
          <div className="col-md-3">
            <div className="my-cards size">
              <img
                src="https://images.unsplash.com/photo-1623749363542-26b4e88c7a22?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1170&q=80"
                className="my-img"
                alt="..."
                onClick={handleSculpture}
              />
              <div className="my-cards-title">
                <h5 className="">Sculpture</h5>
              </div>
            </div>
          </div>
        </div>
        {/* <hr className="featurette-divider mt-5" /> */}
      </div>
    </div>
  );
}

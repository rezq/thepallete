import React from "react";
import axios from "axios";
import { Link } from "react-router-dom";
import { useState, useEffect } from "react";
import Nav from "./components/Nav";
import Type from "./components/Type";
export default function ListProduct(props) {
  const { productList } = props;
  const [prod, setProd] = useState([]);
  const [checkType, setCheckType] = useState(false);

  useEffect(() => {}, [checkType]);

  const search = (key) => {
    // if (key === null) {
    //   let result = await axios.get('http://localhost:8000/api/products');
    //   result = await result.data.data;
    //   console.log(result);
    //   setProd(result);
    // }
    axios.get("https://thepallete.site/api/list-product/" + key).then((res) => {
      let result = res.data.data;
      console.log(result);
      setProd(result);

      setCheckType(true);
    });
    // let result = await axios.get('http://localhost:8000/api/products/' + key);

    // result = result.data.data;
    // console.log(result);
    // setProd(result);

    // setCheckType(true);
  };
  return (
    <div>
      <Nav />
      <div className="container my">
        <div className="row mt-4 justify-content-center">
          <div className="col-md-8">
            <input
              autoComplete="off"
              type="text"
              className="form-control"
              id="exampleInputEmail1"
              aria-describedby="emailHelp"
              placeholder="Search..."
              onChange={(e) => search(e.target.value)}
            ></input>
          </div>
        </div>
        {checkType == true ? (
          <div className="row ">
            {prod.map((product, index) => (
              <div className="col-sm-2 my-3" key={product.id}>
                <div className="card">
                  <img
                    src={`https://thepallete.site/images/${product.img_thumb}`}
                    className="card-img-top"
                    style={{ height: "180px" }}
                    alt="..."
                    onError={(e) => (e.target.style.display = "none")}
                  />

                  <div className="card-body" style={{ height: "120px" }}>
                    {product.status_id == 1 ? (
                      <span className="badge bg-danger">Habis</span>
                    ) : (
                      ""
                    )}
                    {product.status_id == 2 ? (
                      <span className="badge bg-success">Ready</span>
                    ) : (
                      ""
                    )}
                    {product.status_id == 3 ? (
                      <span className="badge bg-warning">Pre-Order</span>
                    ) : (
                      ""
                    )}
                    <div className="row mt-2">
                      <div className="col-md-12">
                        <h6 className="card-title">{product.nama} </h6>
                      </div>
                    </div>

                    <h6 className="card-title mb-3">
                      Rp.{product.price.toLocaleString()}
                    </h6>
                    {/* <p className="card-text mt-">
                  {product.desc.slice(0, 20) + '...'}
                </p> */}
                    <div className="row d-flex justify-content-center"></div>
                  </div>

                  <Link to={"/detail/" + product.id + "/product"}>
                    <div className="d-grid gap-2">
                      <button className="btn btn-primary" type="button">
                        Lihat{" "}
                      </button>
                    </div>
                  </Link>
                </div>
              </div>
            ))}
          </div>
        ) : (
          <Type />
        )}
      </div>
    </div>
  );
}
